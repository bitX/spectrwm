#!/bin/bash
# baraction.sh for spectrwm status bar

## DISK
hdd() {
  hdd=$(df -h | awk 'NR==4{print $3, $5}')
  echo -e " HDD:$hdd"  # Icono de disco duro de Nerdfont (\ue1ba)
}

## RAM
mem() {
  mem=$(free | awk '/Mem/ {printf "%dM/%dM\n", $3 / 1024.0, $2 / 1024.0}')
  echo -e "󰍛 MEM:$mem%"  # Icono de memoria de Nerdfont (\ue21c)
}

## SWAP
swap() {
  swap_total=$(free -m | awk '/Swap/ {print $2}')
  swap_used=$(free -m | awk '/Swap/ {print $3}')
  swap_percent=$((swap_used * 100 / swap_total))
  echo -e "󰯍 SWAP: $swap_used/${swap_total}M ($swap_percent%)"  # Icono de intercambio de Nerdfont (\uf2db)
}

## CPU
cpu() {
  read cpu a b c previdle rest < /proc/stat
  prevtotal=$((a+b+c+previdle))
  sleep 0.5
  read cpu a b c idle rest < /proc/stat
  total=$((a+b+c+idle))
  cpu=$((100*((total-prevtotal)-(idle-previdle))/(total-prevtotal)))
  echo -e "󰻠 CPU: $cpu%"  # Icono de CPU de Nerdfont (\ue22c)
}

## NETWORK
net() {
  wifi=$(ip a | grep wlp2s0 | grep inet | wc -l)

  if [ $wifi -eq 1 ]; then
    echo -e "󱚽 "  # Icono de Wi-Fi de Nerdfont (\uf1eb)
  else
    echo -e "󰈀 "  # Icono de sin conexión de Nerdfont (\uf127)
  fi
}

## BATTERY
bat() {
  batstat=$(cat /sys/class/power_supply/BAT1/status)
  battery=$(cat /sys/class/power_supply/BAT1/capacity)
  if [ "$batstat" = "Charging" ]; then
    batstat="󰢟 "  # Icono de batería cargando de Nerdfont (\uf583)
  elif [ "$batstat" = "Discharging" ]; then
    batstat="󱃍 "  # Icono de batería descargando de Nerdfont (\uf578)
  elif [[ $battery -ge 5 ]] && [[ $battery -le 19 ]]; then
    batstat="󰠈 "  # Icono de batería baja de Nerdfont (\uf244)
  elif [[ $battery -ge 20 ]] && [[ $battery -le 39 ]]; then
    batstat="󰠉 "  # Icono de batería media baja de Nerdfont (\uf243)
  elif [[ $battery -ge 40 ]] && [[ $battery -le 59 ]]; then
    batstat="󰠊 "  # Icono de batería media alta de Nerdfont (\uf242)
  elif [[ $battery -ge 60 ]] && [[ $battery -le 79 ]]; then
    batstat="󰠌 "  # Icono de batería alta de Nerdfont (\uf241)
  elif [[ $battery -ge 80 ]] && [[ $battery -le 95 ]]; then
    batstat="󰠎 "  # Icono de batería llena de Nerdfont (\uf240)
  elif [[ $battery -ge 96 ]] && [[ $battery -le 100 ]]; then
    batstat="󰠐 "  # Icono de batería llena de Nerdfont (\uf240)
  fi

  echo -e "$batstat $battery%"
}

## VOLUME
vol() {
  vol=$(amixer get Master | awk -F'[][]' 'END{ print $4":"$2 }' | sed 's/on://g')
  echo -e " $vol"  # Icono de volumen de Nerdfont (\uf028)
}

SLEEP_SEC=3
# Loop infinito que muestra una línea cada SLEEP_SEC segundos

while :; do
  echo -e "$(cpu) || $(mem) || $(hdd) || $(net) || $(bat) || $(swap) || $(vol)"
  sleep $SLEEP_SEC
done
